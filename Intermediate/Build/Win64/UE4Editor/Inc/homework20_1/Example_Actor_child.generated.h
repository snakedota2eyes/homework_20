// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOMEWORK20_1_Example_Actor_child_generated_h
#error "Example_Actor_child.generated.h already included, missing '#pragma once' in Example_Actor_child.h"
#endif
#define HOMEWORK20_1_Example_Actor_child_generated_h

#define homework20_1_Source_homework20_1_Example_Actor_child_h_15_SPARSE_DATA
#define homework20_1_Source_homework20_1_Example_Actor_child_h_15_RPC_WRAPPERS
#define homework20_1_Source_homework20_1_Example_Actor_child_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define homework20_1_Source_homework20_1_Example_Actor_child_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAExample_Actor_child(); \
	friend struct Z_Construct_UClass_AExample_Actor_child_Statics; \
public: \
	DECLARE_CLASS(AExample_Actor_child, AExample_Actor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework20_1"), NO_API) \
	DECLARE_SERIALIZER(AExample_Actor_child)


#define homework20_1_Source_homework20_1_Example_Actor_child_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAExample_Actor_child(); \
	friend struct Z_Construct_UClass_AExample_Actor_child_Statics; \
public: \
	DECLARE_CLASS(AExample_Actor_child, AExample_Actor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework20_1"), NO_API) \
	DECLARE_SERIALIZER(AExample_Actor_child)


#define homework20_1_Source_homework20_1_Example_Actor_child_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AExample_Actor_child(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AExample_Actor_child) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AExample_Actor_child); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AExample_Actor_child); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AExample_Actor_child(AExample_Actor_child&&); \
	NO_API AExample_Actor_child(const AExample_Actor_child&); \
public:


#define homework20_1_Source_homework20_1_Example_Actor_child_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AExample_Actor_child() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AExample_Actor_child(AExample_Actor_child&&); \
	NO_API AExample_Actor_child(const AExample_Actor_child&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AExample_Actor_child); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AExample_Actor_child); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AExample_Actor_child)


#define homework20_1_Source_homework20_1_Example_Actor_child_h_15_PRIVATE_PROPERTY_OFFSET
#define homework20_1_Source_homework20_1_Example_Actor_child_h_12_PROLOG
#define homework20_1_Source_homework20_1_Example_Actor_child_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework20_1_Source_homework20_1_Example_Actor_child_h_15_PRIVATE_PROPERTY_OFFSET \
	homework20_1_Source_homework20_1_Example_Actor_child_h_15_SPARSE_DATA \
	homework20_1_Source_homework20_1_Example_Actor_child_h_15_RPC_WRAPPERS \
	homework20_1_Source_homework20_1_Example_Actor_child_h_15_INCLASS \
	homework20_1_Source_homework20_1_Example_Actor_child_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define homework20_1_Source_homework20_1_Example_Actor_child_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework20_1_Source_homework20_1_Example_Actor_child_h_15_PRIVATE_PROPERTY_OFFSET \
	homework20_1_Source_homework20_1_Example_Actor_child_h_15_SPARSE_DATA \
	homework20_1_Source_homework20_1_Example_Actor_child_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	homework20_1_Source_homework20_1_Example_Actor_child_h_15_INCLASS_NO_PURE_DECLS \
	homework20_1_Source_homework20_1_Example_Actor_child_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOMEWORK20_1_API UClass* StaticClass<class AExample_Actor_child>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID homework20_1_Source_homework20_1_Example_Actor_child_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
