// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "homework20_1/Example_Actor_child.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeExample_Actor_child() {}
// Cross Module References
	HOMEWORK20_1_API UClass* Z_Construct_UClass_AExample_Actor_child_NoRegister();
	HOMEWORK20_1_API UClass* Z_Construct_UClass_AExample_Actor_child();
	HOMEWORK20_1_API UClass* Z_Construct_UClass_AExample_Actor();
	UPackage* Z_Construct_UPackage__Script_homework20_1();
// End Cross Module References
	void AExample_Actor_child::StaticRegisterNativesAExample_Actor_child()
	{
	}
	UClass* Z_Construct_UClass_AExample_Actor_child_NoRegister()
	{
		return AExample_Actor_child::StaticClass();
	}
	struct Z_Construct_UClass_AExample_Actor_child_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AExample_Actor_child_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AExample_Actor,
		(UObject* (*)())Z_Construct_UPackage__Script_homework20_1,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AExample_Actor_child_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Example_Actor_child.h" },
		{ "ModuleRelativePath", "Example_Actor_child.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AExample_Actor_child_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AExample_Actor_child>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AExample_Actor_child_Statics::ClassParams = {
		&AExample_Actor_child::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AExample_Actor_child_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AExample_Actor_child_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AExample_Actor_child()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AExample_Actor_child_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AExample_Actor_child, 2027026870);
	template<> HOMEWORK20_1_API UClass* StaticClass<AExample_Actor_child>()
	{
		return AExample_Actor_child::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AExample_Actor_child(Z_Construct_UClass_AExample_Actor_child, &AExample_Actor_child::StaticClass, TEXT("/Script/homework20_1"), TEXT("AExample_Actor_child"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AExample_Actor_child);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
