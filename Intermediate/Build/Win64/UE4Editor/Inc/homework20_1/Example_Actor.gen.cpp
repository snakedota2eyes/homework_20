// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "homework20_1/Example_Actor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeExample_Actor() {}
// Cross Module References
	HOMEWORK20_1_API UEnum* Z_Construct_UEnum_homework20_1_EExample();
	UPackage* Z_Construct_UPackage__Script_homework20_1();
	HOMEWORK20_1_API UClass* Z_Construct_UClass_AExample_Actor_NoRegister();
	HOMEWORK20_1_API UClass* Z_Construct_UClass_AExample_Actor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
// End Cross Module References
	static UEnum* EExample_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_homework20_1_EExample, Z_Construct_UPackage__Script_homework20_1(), TEXT("EExample"));
		}
		return Singleton;
	}
	template<> HOMEWORK20_1_API UEnum* StaticEnum<EExample>()
	{
		return EExample_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EExample(EExample_StaticEnum, TEXT("/Script/homework20_1"), TEXT("EExample"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_homework20_1_EExample_Hash() { return 4198143762U; }
	UEnum* Z_Construct_UEnum_homework20_1_EExample()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_homework20_1();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EExample"), 0, Get_Z_Construct_UEnum_homework20_1_EExample_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EExample::E_RED", (int64)EExample::E_RED },
				{ "EExample::E_GREEN", (int64)EExample::E_GREEN },
				{ "EExample::E_BLUE", (int64)EExample::E_BLUE },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "E_BLUE.DisplayName", "'BLUE'" },
				{ "E_BLUE.Name", "EExample::E_BLUE" },
				{ "E_GREEN.DisplayName", "'GREEN'" },
				{ "E_GREEN.Name", "EExample::E_GREEN" },
				{ "E_RED.DisplayName", "'RED'" },
				{ "E_RED.Name", "EExample::E_RED" },
				{ "ModuleRelativePath", "Example_Actor.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_homework20_1,
				nullptr,
				"EExample",
				"EExample",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void AExample_Actor::StaticRegisterNativesAExample_Actor()
	{
	}
	UClass* Z_Construct_UClass_AExample_Actor_NoRegister()
	{
		return AExample_Actor::StaticClass();
	}
	struct Z_Construct_UClass_AExample_Actor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExampleEnumValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ExampleEnumValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ExampleEnumValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AExample_Actor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_homework20_1,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AExample_Actor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/*struct FMyData\n{\n\x09GENERATED_BODY()\npublic:\n\x09UPROPERTY(EditAnywhere, BlueprintReadWrite)\n\x09""EExample EnumValue();\n\x09UPROPERTY(EditAnywhere, BlueprintReadWrite)\n\x09int32 IntValue();\n\x09UPROPERTY(EditAnywhere, BlueprintReadWrite)\n\x09""FString StringValue();\n\x09UPROPERTY(EditAnywhere, BlueprintReadWrite)\n\x09\x09""AActor* ActorPtr;\n\x09""FMyData()\n\x09{\n\x09\x09""EnumValue\x09= EExample::E_GREEN;\n\x09\x09IntValue\x09= 10;\n\x09\x09StringValue = TEXT(\"MyString\");\n\x09\x09""ActorPtr\x09= nullptr;\n\x09}\n};*/" },
		{ "IncludePath", "Example_Actor.h" },
		{ "ModuleRelativePath", "Example_Actor.h" },
		{ "ToolTip", "struct FMyData\n{\n       GENERATED_BODY()\npublic:\n       UPROPERTY(EditAnywhere, BlueprintReadWrite)\n       EExample EnumValue();\n       UPROPERTY(EditAnywhere, BlueprintReadWrite)\n       int32 IntValue();\n       UPROPERTY(EditAnywhere, BlueprintReadWrite)\n       FString StringValue();\n       UPROPERTY(EditAnywhere, BlueprintReadWrite)\n               AActor* ActorPtr;\n       FMyData()\n       {\n               EnumValue       = EExample::E_GREEN;\n               IntValue        = 10;\n               StringValue = TEXT(\"MyString\");\n               ActorPtr        = nullptr;\n       }\n};" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AExample_Actor_Statics::NewProp_ExampleEnumValue_MetaData[] = {
		{ "Category", "Example_Actor" },
		{ "ModuleRelativePath", "Example_Actor.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AExample_Actor_Statics::NewProp_ExampleEnumValue = { "ExampleEnumValue", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AExample_Actor, ExampleEnumValue), Z_Construct_UEnum_homework20_1_EExample, METADATA_PARAMS(Z_Construct_UClass_AExample_Actor_Statics::NewProp_ExampleEnumValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AExample_Actor_Statics::NewProp_ExampleEnumValue_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AExample_Actor_Statics::NewProp_ExampleEnumValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AExample_Actor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AExample_Actor_Statics::NewProp_ExampleEnumValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AExample_Actor_Statics::NewProp_ExampleEnumValue_Underlying,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AExample_Actor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AExample_Actor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AExample_Actor_Statics::ClassParams = {
		&AExample_Actor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AExample_Actor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AExample_Actor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AExample_Actor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AExample_Actor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AExample_Actor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AExample_Actor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AExample_Actor, 4261002733);
	template<> HOMEWORK20_1_API UClass* StaticClass<AExample_Actor>()
	{
		return AExample_Actor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AExample_Actor(Z_Construct_UClass_AExample_Actor, &AExample_Actor::StaticClass, TEXT("/Script/homework20_1"), TEXT("AExample_Actor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AExample_Actor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
