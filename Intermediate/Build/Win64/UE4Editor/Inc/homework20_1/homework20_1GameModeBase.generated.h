// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOMEWORK20_1_homework20_1GameModeBase_generated_h
#error "homework20_1GameModeBase.generated.h already included, missing '#pragma once' in homework20_1GameModeBase.h"
#endif
#define HOMEWORK20_1_homework20_1GameModeBase_generated_h

#define homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_SPARSE_DATA
#define homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_RPC_WRAPPERS
#define homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAhomework20_1GameModeBase(); \
	friend struct Z_Construct_UClass_Ahomework20_1GameModeBase_Statics; \
public: \
	DECLARE_CLASS(Ahomework20_1GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework20_1"), NO_API) \
	DECLARE_SERIALIZER(Ahomework20_1GameModeBase)


#define homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAhomework20_1GameModeBase(); \
	friend struct Z_Construct_UClass_Ahomework20_1GameModeBase_Statics; \
public: \
	DECLARE_CLASS(Ahomework20_1GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework20_1"), NO_API) \
	DECLARE_SERIALIZER(Ahomework20_1GameModeBase)


#define homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Ahomework20_1GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Ahomework20_1GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Ahomework20_1GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Ahomework20_1GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Ahomework20_1GameModeBase(Ahomework20_1GameModeBase&&); \
	NO_API Ahomework20_1GameModeBase(const Ahomework20_1GameModeBase&); \
public:


#define homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Ahomework20_1GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Ahomework20_1GameModeBase(Ahomework20_1GameModeBase&&); \
	NO_API Ahomework20_1GameModeBase(const Ahomework20_1GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Ahomework20_1GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Ahomework20_1GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Ahomework20_1GameModeBase)


#define homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define homework20_1_Source_homework20_1_homework20_1GameModeBase_h_12_PROLOG
#define homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_SPARSE_DATA \
	homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_RPC_WRAPPERS \
	homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_INCLASS \
	homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_SPARSE_DATA \
	homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	homework20_1_Source_homework20_1_homework20_1GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOMEWORK20_1_API UClass* StaticClass<class Ahomework20_1GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID homework20_1_Source_homework20_1_homework20_1GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
