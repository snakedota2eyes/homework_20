// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOMEWORK20_1_Example_Actor_generated_h
#error "Example_Actor.generated.h already included, missing '#pragma once' in Example_Actor.h"
#endif
#define HOMEWORK20_1_Example_Actor_generated_h

#define homework20_1_Source_homework20_1_Example_Actor_h_41_SPARSE_DATA
#define homework20_1_Source_homework20_1_Example_Actor_h_41_RPC_WRAPPERS
#define homework20_1_Source_homework20_1_Example_Actor_h_41_RPC_WRAPPERS_NO_PURE_DECLS
#define homework20_1_Source_homework20_1_Example_Actor_h_41_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAExample_Actor(); \
	friend struct Z_Construct_UClass_AExample_Actor_Statics; \
public: \
	DECLARE_CLASS(AExample_Actor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework20_1"), NO_API) \
	DECLARE_SERIALIZER(AExample_Actor)


#define homework20_1_Source_homework20_1_Example_Actor_h_41_INCLASS \
private: \
	static void StaticRegisterNativesAExample_Actor(); \
	friend struct Z_Construct_UClass_AExample_Actor_Statics; \
public: \
	DECLARE_CLASS(AExample_Actor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework20_1"), NO_API) \
	DECLARE_SERIALIZER(AExample_Actor)


#define homework20_1_Source_homework20_1_Example_Actor_h_41_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AExample_Actor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AExample_Actor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AExample_Actor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AExample_Actor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AExample_Actor(AExample_Actor&&); \
	NO_API AExample_Actor(const AExample_Actor&); \
public:


#define homework20_1_Source_homework20_1_Example_Actor_h_41_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AExample_Actor(AExample_Actor&&); \
	NO_API AExample_Actor(const AExample_Actor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AExample_Actor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AExample_Actor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AExample_Actor)


#define homework20_1_Source_homework20_1_Example_Actor_h_41_PRIVATE_PROPERTY_OFFSET
#define homework20_1_Source_homework20_1_Example_Actor_h_38_PROLOG
#define homework20_1_Source_homework20_1_Example_Actor_h_41_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework20_1_Source_homework20_1_Example_Actor_h_41_PRIVATE_PROPERTY_OFFSET \
	homework20_1_Source_homework20_1_Example_Actor_h_41_SPARSE_DATA \
	homework20_1_Source_homework20_1_Example_Actor_h_41_RPC_WRAPPERS \
	homework20_1_Source_homework20_1_Example_Actor_h_41_INCLASS \
	homework20_1_Source_homework20_1_Example_Actor_h_41_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define homework20_1_Source_homework20_1_Example_Actor_h_41_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework20_1_Source_homework20_1_Example_Actor_h_41_PRIVATE_PROPERTY_OFFSET \
	homework20_1_Source_homework20_1_Example_Actor_h_41_SPARSE_DATA \
	homework20_1_Source_homework20_1_Example_Actor_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
	homework20_1_Source_homework20_1_Example_Actor_h_41_INCLASS_NO_PURE_DECLS \
	homework20_1_Source_homework20_1_Example_Actor_h_41_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOMEWORK20_1_API UClass* StaticClass<class AExample_Actor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID homework20_1_Source_homework20_1_Example_Actor_h


#define FOREACH_ENUM_EEXAMPLE(op) \
	op(EExample::E_RED) \
	op(EExample::E_GREEN) \
	op(EExample::E_BLUE) 

enum class EExample : uint8;
template<> HOMEWORK20_1_API UEnum* StaticEnum<EExample>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
