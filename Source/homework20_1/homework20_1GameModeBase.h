// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "homework20_1GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORK20_1_API Ahomework20_1GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
